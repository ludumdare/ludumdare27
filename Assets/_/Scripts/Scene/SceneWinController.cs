﻿using UnityEngine;
using System.Collections;

public class SceneWinController : MonoBehaviour {

	public tk2dUIItem	StartButton;
	
	void OnEnable() {
		StartButton.OnClick += OnClick;
	}
	
	void OnDisable() {
		StartButton.OnClick -= OnClick;	
	}
	
	void Start() {
		GameMusic.Instance.audio.clip = GameMusic.Instance.PrefabMusicGameEnd;
		GameMusic.Instance.audio.Play();
	}

	void OnClick() {
		Application.LoadLevel("Submit");	
	}
}
