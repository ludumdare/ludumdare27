﻿using UnityEngine;
using System.Collections;

public class SceneHomeController : MonoBehaviour {

	public tk2dUIItem	StartButton;
	public tk2dUIItem	ScoresButton;
	
	void OnEnable() {
		StartButton.OnClick += OnClick;
		ScoresButton.OnClick += DoScores;
	}
	
	void OnDisable() {
		StartButton.OnClick -= OnClick;	
		ScoresButton.OnClick -= DoScores;
	}

	void OnClick() {
		Application.LoadLevel("Hero");	
	}
	
	void DoScores() {
		Application.LoadLevel("Scores");	
	}
}
