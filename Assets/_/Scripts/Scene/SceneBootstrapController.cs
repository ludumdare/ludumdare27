﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneBootstrapController : MonoBehaviour {
	
	public string 	NextLevel;
	
	// Use this for initialization
	void Start () {
		GetHiscore();
		if (Debug.isDebugBuild) {
			Application.LoadLevel(NextLevel);	
		} else {	
			GetUsage();
		}
	}
	
	void GetUsage() {
		string url = "http://www.scoreoid.com/api/getGameData";
		WWWForm form = new WWWForm();
		form.AddField("api_key", 	"e332305002cd38adb3154d4e0a1d0c3a4c21bb85");
		form.AddField("game_id", 	"af7d8bf242");
		form.AddField("response",	"json");
		form.AddField("key",		"usage.playgame");
		
		WWW www = new WWW(url, form);
		HTTP.Instance.Request(www, HttpResponse_GetUsage);
	}
	
	void HttpResponse_GetUsage(WWW www) {
		Log.Normal(Log.Category.Network, www.text);
		if (www.error == null) {
			Log.Normal(Log.Category.Network, string.Format("got http data [{0}]", www.text));
			Dictionary<string, object> _ret = MiniJSON.Json.Deserialize(www.text) as Dictionary<string, object>;
			Log.Normal(string.Format("{0}", _ret["usage.playgame"]));
			string _u = (string) _ret["usage.playgame"];
			int playgame = int.Parse(_u);
			SetUsage(playgame);
		}
	}
	
	void SetUsage(int playgame) {
		
		playgame++;
		
		string url = "http://www.scoreoid.com/api/setGameData";
		WWWForm form = new WWWForm();
		form.AddField("api_key", 	"e332305002cd38adb3154d4e0a1d0c3a4c21bb85");
		form.AddField("game_id", 	"af7d8bf242");
		form.AddField("response",	"json");
		form.AddField("key",		"usage.playgame");
		form.AddField("value",		playgame.ToString());
		
		WWW www = new WWW(url, form);
		HTTP.Instance.Request(www, HttpResponse_SetUsage);
	}
	
	void HttpResponse_SetUsage(WWW www) {
		Log.Normal(Log.Category.Network, www.text);
		Application.LoadLevel(NextLevel);	
	}
	
	void GetHiscore() {
		string url = "http://www.scoreoid.com/api/getGameTop";
		WWWForm form = new WWWForm();
		form.AddField("api_key", 	"e332305002cd38adb3154d4e0a1d0c3a4c21bb85");
		form.AddField("game_id", 	"af7d8bf242");
		form.AddField("field",		"best_score");
		form.AddField("response",	"json");
		
		WWW www = new WWW(url, form);
		HTTP.Instance.Request(www, HttpResponse_GetHiscore);
	}
	
	void HttpResponse_GetHiscore(WWW www) {
		Log.Normal(Log.Category.Network, www.text);
		if (www.error == null) {
			Log.Normal(Log.Category.Network, string.Format("got http data [{0}]", www.text));
			Dictionary<string, object> _ret = MiniJSON.Json.Deserialize(www.text) as Dictionary<string, object>;
			Log.Normal(string.Format("{0}", _ret["best_score"]));
			GameManager.Instance.HiScore = (int) (long) _ret["best_score"];
			//SetUsage(playgame);
		}
	}
	
	
}
