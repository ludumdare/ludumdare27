﻿using UnityEngine;
using System.Collections;

public class ScenesSubmitController : MonoBehaviour {
	
	public tk2dUIItem	ButtonSubmit;
	public tk2dTextMesh TextScore;
	public tk2dUITextInput TextName;

	void OnEnable() {
		ButtonSubmit.OnClick += OnClick;	
	}
	
	void OnDisable() {
		ButtonSubmit.OnClick -= OnClick;
	}
	
	// Use this for initialization
	void Start () {
	
		TextScore.text = GameManager.Instance.Score.ToString();
		TextName.Text = GameManager.Instance.LastSubmitName;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick() {
		
		GameManager.Instance.LastSubmitName = TextName.Text;
		
		string url = "http://www.scoreoid.com/api/createScore";
		WWWForm form = new WWWForm();
		form.AddField("api_key", 	"e332305002cd38adb3154d4e0a1d0c3a4c21bb85");
		form.AddField("game_id", 	"af7d8bf242");
		form.AddField("response",	"json");
		form.AddField("score",		GameManager.Instance.Score.ToString());
		form.AddField("username",	TextName.Text);
		form.AddField("platform",	"web");
		
		WWW www = new WWW(url, form);
		HTTP.Instance.Request(www, HttpResponse);
	}
	
	void HttpResponse(WWW www) {
		Log.Normal(Log.Category.Network, string.Format("got http data [{0}]", www.text));
		Application.LoadLevel("Home");	
	}
}
