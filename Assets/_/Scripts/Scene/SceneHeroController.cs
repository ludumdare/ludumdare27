﻿using UnityEngine;
using System.Collections;

public class SceneHeroController : MonoBehaviour {
	
	public tk2dUIItem	RollButton;
	
	
	void OnEnable() {
		RollButton.OnClick += OnClick;
	}
	
	void OnDisable() {
		RollButton.OnClick -= OnClick;
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick() {
		Application.LoadLevel(Application.loadedLevel);	
	}
}
