using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneScoresController : MonoBehaviour {
	
	public tk2dUIItem		StartButton;
	public tk2dTextMesh		TextPlayers;
	public tk2dTextMesh		TextScores;

	void OnEnable() {
		StartButton.OnClick += OnClick;
	}
	
	void OnDisable() {
		StartButton.OnClick -= OnClick;	
	}
	
	void Start() {
		
		string url = "http://www.scoreoid.com/api/getBestScores";
		WWWForm form = new WWWForm();
		form.AddField("api_key", 	"e332305002cd38adb3154d4e0a1d0c3a4c21bb85");
		form.AddField("game_id", 	"af7d8bf242");
		form.AddField("response",	"json");
		form.AddField("order_by",	"score");
		form.AddField("limit",		"10");
		
		WWW www = new WWW(url, form);
		HTTP.Instance.Request(www, HttpResponse);
		
	}
	
	void HttpResponse(WWW www) {
		Log.Normal(Log.Category.Network, string.Format("got http data [{0}]", www.text));
		UpdateScores(www.text);
	}

	void OnClick() {
		Application.LoadLevel("Home");	
	}
	
	void UpdateScores(string data) {
		
		//Dictionary<string, object> _scores =  MiniJSON.Json.Deserialize(data) as Dictionary<string, object>;
		List<object> _scores = MiniJSON.Json.Deserialize(data) as List<object>;
		int i=0;
		string scores = "";
		string players = "";
		foreach(System.Object obj in _scores) {
			Dictionary<string,object> _score = (Dictionary<string, object>) obj;
			Dictionary<string,object> _scoreplayer = (Dictionary<string, object>) _score["Player"];
			Dictionary<string,object> _playerscore = (Dictionary<string, object>) _score["Score"];
			Log.Normal(string.Format("player {0} score {1}", _scoreplayer["username"], _playerscore["score"]));
			i++;
			players += i.ToString() + ". " + _scoreplayer["username"] + "\n";
			scores += _playerscore["score"] + "\n";
		}
		
		Log.Normal(players);
		
		TextPlayers.text = players;
		TextScores.text = scores;
	}
	
	
	
}
