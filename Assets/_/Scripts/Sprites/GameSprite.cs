using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameSprite : MonoBehaviour {
	
	public float			PosX;
	public float			PosY;
	public float			Speed;
	
	private Room			m_room;
	private float			m_xpos;
	private float			m_ypos;
	private float			m_base = 1f;
	private float			m_ymax;
	private float 			m_xmax;
	private float			m_ymin;
	private float			m_xmin;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void SetRoom(Room room) { 
		m_room = room; 
		PosX = Mathf.Ceil(transform.position.x/m_base);
		PosY = Mathf.Ceil(transform.position.y/m_base);
		m_xmin = m_room.RoomTopPos.x; //m_room.RoomPosition.x * 16;
		m_ymin = m_room.RoomTopPos.y; //m_room.RoomPosition.y * 16;
		m_xmax = m_room.RoomBotPos.x; //m_xmin + (m_room.RoomWidth * 16);
		m_ymax = m_room.RoomBotPos.y; //m_ymin + (m_room.RoomDepth * 16);
		
		Log.Watch("xmin", m_xmin);
		Log.Watch("xmax", m_xmax);
		Log.Watch("ymin", m_ymin);
		Log.Watch("ymax", m_ymax);
	}
	
	public void MoveLeft(int step) {
		m_xpos = -1;
		m_ypos = 0;
		_Move();
	}
	
	public void MoveRight(int step) {
		m_xpos = 1;
		m_ypos = 0;
		_Move();
	}
	
	public void MoveDown(int step) {
		m_xpos = 0;
		m_ypos = -1;
		_Move();
	}
	
	public void MoveUp(int step) {
		m_xpos = 0;
		m_ypos = 1;
		_Move();
	}
	
	public void MoveBy(int type) {
		if (type == 1) MoveUp(0);
		if (type == 2) MoveRight(0);
		if (type == 3) MoveDown(0);
		if (type == 4) MoveLeft(0);
	}
	
	
	// Move the sprite on the game grid, not
	// actual vector coordinates
	
	public void MoveTo( Vector3 vector ) {
		
		//m_xpos = vector.x;
		//m_ypos = vector.y;
		
		//_Move();		
		Vector3 _v = vector - transform.position;
		_v.Normalize();
		//Log.Normal(_v.ToString());
		//Log.Normal(string.Format("MoveTo = ", _v));
		//transform.Translate(_v * Speed * Time.deltaTime);
		//transform.position += _v * Speed * Time.deltaTime;
		//rigidbody.MovePosition(rigidbody.position + _v * Speed * Time.deltaTime);
		rigidbody.velocity = _v * Speed;
	}
	
	public void MoveToRandom() {
		
		//m_xpos = vector.x;
		//m_ypos = vector.y;
		
		//_Move();	
		float _x = Random.Range(m_xmin, m_xmax);
		float _y = Random.Range(m_ymin, m_ymax);
		Vector3 _r = new Vector3(_x, _y, -5);
		Vector3 _v = _r - transform.position;
		_v.Normalize();
		//Log.Normal(_v.ToString());
		//Log.Normal(string.Format("MoveTo = ", _v));
		//transform.Translate(_v * Speed * Time.deltaTime);
		//transform.position += _v * Speed * Time.deltaTime;
		//rigidbody.MovePosition(rigidbody.position + _v * Speed * Time.deltaTime);
		rigidbody.velocity = _v * Speed;
	}
	
	
	
	private void _Move() {
		
		//m_world = GameController.Instance.World;
		//m_ymax = m_world.WorldDepth * 5;

		//PosX = m_xpos;
		//PosY = m_ypos;
		
		float _x = ( (PosX + (m_xpos * Speed)) );
		float _y = ( (PosY + (m_ypos * Speed)) );
		
		Log.Watch("x", _x);
		Log.Watch("y", _y);
		
		if ( _x <= m_xmin ) return;
		if ( _x >= m_xmax ) return;
		if ( _y <= m_ymin ) return;
		if ( _y >= m_ymax ) return;
		
		PosX = _x;
		PosY = _y;
		
		float _z = -5;
		
		Vector3 _vector = new Vector3( _x, _y, _z );
		//iTween.MoveTo(gameObject, _vector, 1);
		//HOTween.To( transform, 1, new TweenParms().Prop("position", _vector) );
		transform.position = _vector;
		//SendMessage("MoveComplete", SendMessageOptions.DontRequireReceiver);
	}
	
	public int x {
		get { return (int) m_xpos; }
	}

	public int y {
		get { return (int) m_ypos; }
	}

}
