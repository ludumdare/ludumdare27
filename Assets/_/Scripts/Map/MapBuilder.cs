﻿using UnityEngine;
using System.Collections;

public class MapBuilder {

	private int m_batch = 0;
	private int m_batchWall = 0;
	private tk2dStaticSpriteBatcher m_batcher;
	private tk2dStaticSpriteBatcher m_batcherWalls;
	
	public void Build(Map map) {
		
		GameObject x = GameObject.Find("Batchers");
		GameObject.Destroy(x);
		GameObject g = new GameObject("Batchers");
		
		foreach(Room room in map.MapRooms) {
			Log.Normal("preparing sprite batcher");
			GameObject go = new GameObject("SpriteBatcher");
			go.transform.parent = g.transform;
			go.transform.position = new Vector3(go.transform.position.x, go.transform.position.y, 5);
			m_batcher = go.AddComponent<tk2dStaticSpriteBatcher>();
			GameObject go1 = new GameObject("SpriteBatcherWalls");
			m_batcherWalls = go1.AddComponent<tk2dStaticSpriteBatcher>();
			go1.transform.parent = g.transform;			
			int _tiles = 0;
			int _wallTiles = 0;
			Log.Normal(string.Format("there are {0} rooms in the map", map.MapRooms.Count));
			//foreach(Room room in map.MapRooms) {
			//	Log.Minor("adding tiles from room " + room.RoomName);
				_tiles += (room.RoomWidth * room.RoomDepth);
				_wallTiles += ( ( (room.RoomWidth/2) + 2 ) * 2) + ( ( (room.RoomDepth/2)) * 2);
			//}
			Log.Minor("sprite batcher needs room for " + _tiles + " tiles");
			m_batcher.batchedSprites = new tk2dBatchedSprite[_tiles];
			Log.Minor("sprite batcher needs room for " + _wallTiles + " wall tiles");
			m_batcherWalls.batchedSprites = new tk2dBatchedSprite[_wallTiles];
			m_batch = 0;
			m_batchWall = 0;
			BuildRoom(room);	
	        m_batcher.SetFlag( tk2dStaticSpriteBatcher.Flags.GenerateCollider, false );
	        m_batcher.Build();
	        m_batcherWalls.SetFlag( tk2dStaticSpriteBatcher.Flags.GenerateCollider, false );
	        m_batcherWalls.Build();
		}
		
	}
	
	// lot of really bad code going on here
	void BuildRoom(Room room) {
		
		Log.Normal(string.Format("building room {0}", room.RoomName));
		Log.Normal(string.Format("room position is {0}", room.RoomPosition));
		
		float _marx = (room.RoomPosition.x * 5) * 20;
		float _mary = (room.RoomPosition.y * 5) * 20;
		float _offx = ((room.RoomPosition.x * room.RoomWidth) * 20) + _marx;
		float _offy = ((room.RoomPosition.y * room.RoomDepth) * 20) + _mary;
		
		float _cx = ((room.RoomPosition.x * (room.RoomWidth/2)) * 20) + _marx;
		float _cy = ((room.RoomPosition.y * (room.RoomDepth/2)) * 20) + _mary;
		
        for (int j = 0; j < (room.RoomDepth); ++j) {
        	for (int k = 0; k < (room.RoomWidth); ++k) {
	            tk2dBatchedSprite bs = new tk2dBatchedSprite();
	            // assign sprite collection and sprite Id for this batched sprite
	            bs.spriteCollection = room.RoomTheme.StyleSpriteCollection;
	            bs.spriteId = room.RoomTheme.StyleStartingId;
	            Vector3 pos = new Vector3( ((k+2) * 20) + _offx , ((j*20) + 32) + _offy, 0);
	            bs.relativeMatrix.SetTRS(pos, Quaternion.identity, Vector3.one);
	            m_batcher.batchedSprites[m_batch] = bs;
				++m_batch;
			}
		}
		
		// build top/bot walls
		
		for (int j = 0; j < ( (room.RoomWidth/2) + 2); ++j) {
            tk2dBatchedSprite bs = new tk2dBatchedSprite();
            bs.spriteCollection = room.RoomTheme.StyleSpriteCollection;
            bs.spriteId = 287;
			if (j==0) bs.spriteId = 286;
			if (j>(room.RoomWidth/2)) bs.spriteId = 288;
            Vector3 pos = new Vector3( (j*40) + _offx + 10, _offy, 0);
            bs.relativeMatrix.SetTRS(pos, Quaternion.identity, Vector3.one);
            m_batcherWalls.batchedSprites[m_batchWall] = bs;
			++m_batchWall;
            tk2dBatchedSprite bs2 = new tk2dBatchedSprite();
            bs2.spriteCollection = room.RoomTheme.StyleSpriteCollection;
            Vector3 pos2 = new Vector3( (j*40) + _offx + 10, ((room.RoomDepth/2)*40) + _offy + 40, 0);
            bs2.spriteId = 261;
			if (j==0) bs2.spriteId = 260;
			if (j>(room.RoomWidth/2)) bs2.spriteId = 262;
            bs2.relativeMatrix.SetTRS(pos2, Quaternion.identity, Vector3.one);
            m_batcherWalls.batchedSprites[m_batchWall] = bs2;
			++m_batchWall;
		}
		
		// build side walls
		

		for (int j = 0; j < ( (room.RoomDepth/2)); ++j) {
            tk2dBatchedSprite bs = new tk2dBatchedSprite();
            bs.spriteCollection = room.RoomTheme.StyleSpriteCollection;
            bs.spriteId = 273;
            Vector3 pos = new Vector3( _offx + 10, (j*40) + _offy + 40, 0);
            bs.relativeMatrix.SetTRS(pos, Quaternion.identity, Vector3.one);
            m_batcherWalls.batchedSprites[m_batchWall] = bs;
			++m_batchWall;
            tk2dBatchedSprite bs2 = new tk2dBatchedSprite();
            bs2.spriteCollection = room.RoomTheme.StyleSpriteCollection;
            Vector3 pos2 = new Vector3( _offx + 10 + (((room.RoomWidth/2)+1) * 40), (j*40) + _offy + 40, 0);
            bs2.spriteId = 275;
            bs2.relativeMatrix.SetTRS(pos2, Quaternion.identity, Vector3.one);
            m_batcherWalls.batchedSprites[m_batchWall] = bs2;
			++m_batchWall;
		}
		
		Log.Watch("batchwall", m_batchWall);
		
	}
}

	/*
	void Awake() {
	
		GameObject go = new GameObject();
		tk2dStaticSpriteBatcher batcher = go.AddComponent<tk2dStaticSpriteBatcher>();
		batcher.batchedSprites = new tk2dBatchedSprite[(RoomWidth*RoomDepth)];
		
		Log.Watch("size", RoomDepth*RoomWidth);
		
		int i = 0;
		
        for (int j = 0; j < RoomWidth; ++j) {
			
            tk2dBatchedSprite bs = new tk2dBatchedSprite();
            // assign sprite collection and sprite Id for this batched sprite
            bs.spriteCollection = RoomSprites;
            bs.spriteId = RoomTile+1;
            Vector3 pos = new Vector3(((j+1)*20) ,(RoomDepth * 20), 0);
            // Assign the relative matrix. Use this in place of bs.position
            bs.relativeMatrix.SetTRS(pos, Quaternion.identity, Vector3.one);
            batcher.batchedSprites[i] = bs;
			++i;
			
            bs = new tk2dBatchedSprite();
            // assign sprite collection and sprite Id for this batched sprite
            bs.spriteCollection = RoomSprites;
            bs.spriteId = RoomTile+97;
            pos = new Vector3(((j+1)*20) ,20, 0);
            // Assign the relative matrix. Use this in place of bs.position
            bs.relativeMatrix.SetTRS(pos, Quaternion.identity, Vector3.one);
            batcher.batchedSprites[i] = bs;
			++i;
			
			Log.Normal("i,j = " + i + ", " + j);
        }

        for (int j = 0; j < (RoomDepth -2); ++j) {
			
            tk2dBatchedSprite bs = new tk2dBatchedSprite();
            // assign sprite collection and sprite Id for this batched sprite
            bs.spriteCollection = RoomSprites;
            bs.spriteId = RoomTile+64;
            Vector3 pos = new Vector3(20 ,((j+2)*20), 0);
            // Assign the relative matrix. Use this in place of bs.position
            bs.relativeMatrix.SetTRS(pos, Quaternion.identity, Vector3.one);
            batcher.batchedSprites[i] = bs;
			++i;
			
            bs = new tk2dBatchedSprite();
            // assign sprite collection and sprite Id for this batched sprite
            bs.spriteCollection = RoomSprites;
            bs.spriteId = RoomTile+67;
            pos = new Vector3(((RoomWidth)*20) ,((j+2)*20), 0);
            // Assign the relative matrix. Use this in place of bs.position
            bs.relativeMatrix.SetTRS(pos, Quaternion.identity, Vector3.one);
            batcher.batchedSprites[i] = bs;
			++i;
			Log.Normal("i,j = " + i + ", " + j);
        }
		
		// draw the remaining floor
		
        for (int j = 0; j < (RoomWidth -2); ++j) {
        	for (int k = 0; k < (RoomDepth -2); ++k) {
	            tk2dBatchedSprite bs = new tk2dBatchedSprite();
	            // assign sprite collection and sprite Id for this batched sprite
	            bs.spriteCollection = RoomSprites;
	            bs.spriteId = RoomTile+33;
	            Vector3 pos = new Vector3(((j+2)*20) ,(k*20)+32, 0);
	            // Assign the relative matrix. Use this in place of bs.position
	            bs.relativeMatrix.SetTRS(pos, Quaternion.identity, Vector3.one);
	            batcher.batchedSprites[i] = bs;
				++i;
				Log.Normal("j,k = " + j + ", " + k);
				Log.Watch("i", i);
				Log.Watch("j", j);
				Log.Watch("k", k);
			}
		}
		
        // Don't create colliders when you don't need them. It is very expensive to
        // generate colliders at runtime.
        batcher.SetFlag( tk2dStaticSpriteBatcher.Flags.GenerateCollider, false );

        batcher.Build();
	}
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	*/
	
