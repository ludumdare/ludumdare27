﻿
// Copyright 2013 SpockerDotNet LLC

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/**
 * 	
 * 
 * 
 **/

public class Map {
	
	
	public List<Room> 	MapRooms 		{ get; set; }		// the rooms on the map
	public List<Room> 	MapBossRooms	{ get; set; }		// boss rooms
	
	private List<string> m_room;	// random room names
	private List<string> m_color;	// random room colors
	private List<string> m_adj;		// random room adjective
	private List<string> m_of;		// random room of-word
	
	private System.Random m_random;
	
	private MapLevel m_level;
	private int m_num;
	
	public Map() {
		
		m_random = new System.Random();
		// room names
		
		m_room = new List<string>()
		{
			"Locker",
			"Kitchen",
			"Den",
			"Cardroom",
			"Hall",
			"Refractory",
			"Closet",
			"Meat Locker",
			"Torture Chamber",
			"Pantry",
			"Jail",
			"Studio",
			"Pit",
			"Smokehouse",
			"Bedroom",
			"Garden",
			"Basement",
			"Hole",
			"Portal"
		};
		
		m_color = new List<string>()
		{
			"Gold",
			"Silver",
			"Bronze",
			"Brass",
			"Rose",
			"Mauve",
			"Red",
			"Blue",
			"Yellow",
			"Green",
			"Sapphire",
			"Diamond"
		};
		
		m_adj = new List<string>()
		{
			"The Unholy",
			"The Sable",
			"The Loathsome",
			"The Interdimensional",
			"The Encusted",
			"The Defiled",
			"The Stripped",
			"The Destroyed",
			"The Holy",
			"The Heavenly",
			"The Hellish",
			"The Rubber",
			"The Awesome",
			"The Legendary"
		};
		
		m_of = new List<string>()
		{
			"of Filth",
			"of the Desolate",
			"of Lies",
			"of Truths",
			"of Sacrifice",
			"of Loss",
			"of Worship",
			"of Denial",
			"of Latex",
			"of Awesomeness",
			"of Legends"
		};
		
	}

	public void Generate(MapLevel level) {
		
		m_level = level;
		
		// create random room name
		MapRooms = new List<Room>();
		MapBossRooms = new List<Room>();
		
		CreateRooms(level.MapRooms);
		CreateBossRooms(level.MapBossRooms);
		
	}
	
	public string CreateName() {
	
		string _name = "";
		
		// add adj
		_name += m_adj[ m_random.Next(m_adj.Count)];
		
		// add color
		_name += " " + m_color[ m_random.Next(m_color.Count)];
		
		// add room
		_name += " " + m_room[ m_random.Next(m_room.Count)];

		// add of-room
		_name += " " + m_of[ m_random.Next(m_of.Count)];
		
		Log.Watch("name", _name);
		
		return _name;
		
	}
	
	public void CreateBossRooms(int rooms) {
		
		RoomStyle _style = new RoomStyle();
		_style.StyleSpriteCollection = GameManager.Instance.RoomSprites;
		_style.StyleStartingId = 116;
		
		for (int i = 0; i < rooms; i++ ) {
			Room _room = CreateRoom();
			_room.RoomBoss = true;
			_room.RoomTheme = _style;
			MapRooms.Add(_room);	
		}
	}
	
	public void CreateRooms(int rooms) {
		
		Log.Normal(string.Format("adding {0} rooms to the map", rooms));
		
		// starting room is always at 0,0
		
		Room _room1 = new Room();
		_room1.RoomName = "Level Entrance";
		_room1.RoomPosition = new Vector2(0,0);
		_room1.RoomWidth = 32;
		_room1.RoomDepth = 32;
		_room1.RoomNumber = m_num++;
		_room1.RoomComplete = true;
		
		RoomStyle _style = new RoomStyle();
		_style.StyleSpriteCollection = GameManager.Instance.RoomSprites;
		_style.StyleStartingId = 7;
		_room1.RoomTheme = _style;
		
		GameManager.Instance.SetCurrentRoom(_room1);
		
		MapRooms.Add(_room1);
	
		for (int i = 0; i < rooms; i++ ) {
			Room _room = CreateRoom();
			_room.RoomTheme = _style;
			_room.RoomNumber = m_num++;
			MapRooms.Add(_room);	
		}
		
	}
	
	public Room CreateRoom() {
		
		int _failcount = 0;
		bool _fail = false;
		bool _found = false;
		bool _samepos = false;
		
		Room _room = new Room();
		_room.RoomName = CreateName();
		_room.RoomWidth = 32;
		_room.RoomDepth = 32;
		_room.RoomMonsters = m_level.MapMonsters;
		
		Log.Normal(string.Format("AA finding position for room {0}", _room.RoomName));

		while ( !_fail & !_found ) {
			
			// to prevent endless loop
			_failcount++;
			if (_failcount > 100) _fail = true;
			
			Log.Normal(string.Format("AA try number {0}", _failcount));
			
			// pick a room at random
			Room _r = MapRooms[m_random.Next(MapRooms.Count)];
			Log.Normal(string.Format("AA trying to add {1} off of room {0}", _r.RoomName, _room.RoomName));
					
			// are there any openings?
			List<int> _doors = new List<int>();
			if (!_r.RoomDoors[0]) _doors.Add(1);
			if (!_r.RoomDoors[1]) _doors.Add(2);
			if (!_r.RoomDoors[2]) _doors.Add(3);
			if (!_r.RoomDoors[3]) _doors.Add(4);
			
			// pick a random location
			Log.Normal(string.Format("AA there are {0} available doors in {1}", _doors.Count, _r.RoomName));
			int _door = 0;
			if (_doors.Count > 0) {
				_door = _doors[m_random.Next(_doors.Count)];	
				_found = true;
			}
			
			if (_found) {
				
				// get new position
				
				Log.Normal(string.Format("AA adding room off of door {0} from {1}", _door, _r.RoomName));
				int _x = 0;
				int _y = 0;
				int _newdoor = 0;
				
				switch (_door-1) 
				{
				case 0:
					_x = 0; _y = 1;	_newdoor = 2;
					break;
					
				case 1:
					_x = 1; _y = 0; _newdoor = 3;
					break;
					
				case 2:
					_x = 0; _y = -1; _newdoor = 0;
					break;
					
				case 3:
					_x = -1; _y = 0; _newdoor = 1;
					break;
				}
			
				Log.Minor(string.Format("AA new x,y is {0},{1}", _x, _y));
			
				// set position relative to door
				Vector2 _pos = new Vector2( _r.RoomPosition.x + _x, _r.RoomPosition.y + _y);
				
				_samepos = false;
				// see if there is a room here already
				foreach(Room room in MapRooms) {
					Log.Normal(string.Format("AA checking to see if room is at position {0}",_pos));
					if (_pos == room.RoomPosition) {
						Log.Critical(string.Format("AA the room {0} is already at position {1}", room.RoomName, _pos));
						_found = false;
						_samepos = true;
					}
				}
				
				if ( !_samepos) {
					
					Log.Normal(string.Format("AA adding room {0} to position {1}", _room.RoomName, _pos));
					_room.RoomPosition = _pos;
					_room.RoomDoors[_newdoor] = true;
					_room.RoomAttached[_newdoor] = _r;
					
					// update door of selected room
					Log.Normal(string.Format("AA updating door {0}", _door));
					_r.RoomDoors[_door-1] = true;
					_r.RoomAttached[_door-1] = _room;
				}
			}
		}
		
		if (_fail) Log.Critical("AA could not add room");
		
		return _room;
	}
	
}
