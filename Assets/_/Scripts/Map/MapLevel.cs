﻿using UnityEngine;
using System.Collections;

public class MapLevel {
	
	public int	MapRooms 		{ get; set; }		// number of rooms in the map
	public int	MapBossRooms	{ get; set; }		// number of boss rooms
	public int	MapMonsters		{ get; set; }		// number of monsters per room
	
}
