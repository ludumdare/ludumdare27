using UnityEngine;
using System.Collections;

public class GamePlayerController : MonoSingleton<GamePlayerController> {
	
	//public float			KeyDelay;
	
	public  GameObject		Player;
	public	GameObject		Missle;
	public  float			ShootDelay;
	
	private GameSprite		m_sprite;
	private bool			m_delay = false;
	private bool			m_kill = false;
	private bool			m_disable = false;
	
	// Use this for initialization
	void Start () {
		Log.Normal("player is " + Player);
		m_sprite = Player.GetComponentInChildren<GameSprite>();
	}
	
	// Update is called once per frame
	
	void Update () {
		
		// capture keypress
		
		if ( Input.GetKey ( KeyCode.LeftArrow) || Input.GetKey ( KeyCode.A)) {
			MoveLeft();
		}
		
		if ( Input.GetKey ( KeyCode.RightArrow ) || Input.GetKey ( KeyCode.D) ) {
			MoveRight();
		}
		
		if ( Input.GetKey ( KeyCode.UpArrow )  || Input.GetKey ( KeyCode.W)) {
			MoveUp();
		}
		
		if ( Input.GetKey ( KeyCode.DownArrow ) || Input.GetKey ( KeyCode.S) ) {
			MoveDown();
		}
		
		if ( Input.GetMouseButton(0) ) {
			Shoot();	
		}
		
	}
	
	private void Shoot() {
		if (m_delay) return;
		Vector3 _pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		_pos = new Vector3(_pos.x, _pos.y, -2);
		Log.Watch("world pos", _pos);
		GameObject _missle = (GameObject) Instantiate(GamePrefab.Instance.PrefabPlayerMissle, Player.transform.position, Quaternion.identity);
		_missle.transform.position = new Vector3(_missle.transform.position.x, _missle.transform.position.y - 64, _missle.transform.position.z);
		Vector3 _dir = Player.transform.position - _pos;
		_dir.Normalize();
		_missle.GetComponentInChildren<Rigidbody>().velocity = _dir * -500;
		m_delay = true;
		StartCoroutine(Delay());
		//_missle.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, -1);
		//_missle.GetComponentInChildren<Rigidbody>().velocity = transform.forward * 150;
		
		
		
	}
	
	private bool MoveLeft() {
		
		//if ( !GameController.Instance.World.IsPassableTerrain( m_sprite.x - 1, m_sprite.y ) ) 
		//	{ return false; }
		
		m_sprite.MoveLeft(1);
		return true;
	}
	
	private bool MoveRight() {
		
		//if ( !GameController.Instance.World.IsPassableTerrain( m_sprite.x + 1, m_sprite.y ) ) 
		//	{ return false; }
		
		m_sprite.MoveRight(1);
		return true;
	}
	
	private bool MoveUp() {
		
		//if ( !GameController.Instance.World.IsPassableTerrain( m_sprite.x , m_sprite.y + 1 ) ) 
		//	{ return false; }
		
		m_sprite.MoveUp(1);
		return true;
	}
	
	private bool MoveDown() {
		
		//if ( !GameController.Instance.World.IsPassableTerrain( m_sprite.x, m_sprite.y - 1 ) ) 
		//	{ return false; }
		
		m_sprite.MoveDown(1);
		return true;
	}
	
	IEnumerator Delay() {
		yield return new WaitForSeconds(ShootDelay);
		m_delay = false;
	}
	
}
