﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	
	public bool		PlayerHasKey;
	
	public AudioClip	SoundKey;
	public AudioClip	SoundHit;
	public AudioClip	SoundGun;

	void OnTriggerEnter(Collider other) {
	
		if (other.name == "TreasureKey" ) {
			PlayerHasKey = true;
			GameManager.Instance.HasKey = true;
			audio.clip = GameSound.Instance.PlayerKey;
			audio.Play();	
			Destroy(other.gameObject);
			foreach(GameObject obj in GameObject.FindGameObjectsWithTag("Door")) {
				obj.GetComponentInChildren<GameDoor>().UnlockDoor();
			}
		}
		
		if (other.tag == "Coin") {
			GameManager.Instance.AddCoins(other.GetComponentInChildren<GameCoin>().Coins);
			audio.clip = GameSound.Instance.PlayerCoins;
			audio.Play();	
			Destroy(other.gameObject);
		}
		
	}
	
	void OnCollisionEnter(Collision collision) {
		Log.Normal("player collision");
		if (collision.gameObject.tag == "Enemy") {
			GameManager.Instance.PlayerDamage(1);	
		}
		
	}
}
