﻿using UnityEngine;
using System.Collections;

public class Player {

	public string			PlayerName 				{ get; set; }
	public int				PlayerWeapon			{ get; set; }
	public int				PlayerAvatar			{ get; set; }
	public CharacterClass	PlayerCharacterClass	{ get; set; }
}
