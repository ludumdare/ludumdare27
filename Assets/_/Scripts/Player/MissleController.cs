﻿using UnityEngine;
using System.Collections;

public class MissleController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(Kill());	
	}
	
	IEnumerator Kill() {
		yield return new WaitForSeconds(2);
		Destroy(gameObject);
	}
}
