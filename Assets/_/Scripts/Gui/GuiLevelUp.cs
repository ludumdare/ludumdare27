﻿using UnityEngine;
using System.Collections;

public class GuiLevelUp : MonoSingleton<GuiLevelUp> {
	
	public tk2dUIItem	ReadyButton;
	public tk2dUIItem	AddArmor;
	public tk2dUIItem	AddHitpoints;
	public tk2dUIItem	AddLuck;
	public tk2dUIItem	AddSpeed;
	public tk2dUIItem	AddStrength;
	public tk2dUIItem	AddWeapon;
	public tk2dTextMesh	TextPoints;
	public GameObject	ButtonContainer;
	
	public int			PointsAvailable;
	
	void OnEnable() {
		AddArmor.OnClick += DoAddArmor;
		AddHitpoints.OnClick += DoAddHitpoints;
		AddLuck.OnClick += DoAddLuck;
		AddSpeed.OnClick += DoAddSpeed;
		AddStrength.OnClick += DoAddStrength;
		AddWeapon.OnClick += DoAddWeapon;
		ReadyButton.OnClick += DoReady;
	}
	
	void OnDisable() {
		AddArmor.OnClick -= DoAddArmor;
		AddHitpoints.OnClick -= DoAddHitpoints;
		AddLuck.OnClick -= DoAddLuck;
		AddSpeed.OnClick -= DoAddSpeed;
		AddStrength.OnClick -= DoAddStrength;
		AddWeapon.OnClick -= DoAddWeapon;
		ReadyButton.OnClick -= DoReady;
	}
	
	public void Show() {
		Log.Normal("show");
		transform.localPosition = new Vector3(0,0,0);
		ButtonContainer.transform.localPosition = new Vector3(0,0,0);
		TextPoints.text = string.Format("Points Available: {0}", PointsAvailable);
	}
	
	public void Hide() {
		Log.Normal("hide");
		transform.localPosition = new Vector3(1000,1000,1000);
	}
	
	void DoAddArmor() {
		if (PointsAvailable > 0) GameManager.Instance.HeroClass.CharacterArmour += 1;
		RemovePoints();
		GuiStats.Instance.Refresh();
	}
	
	void DoAddHitpoints() {
		if (PointsAvailable > 0) GameManager.Instance.HeroClass.CharacterHitPoints += 1;
		RemovePoints();
		GuiStats.Instance.Refresh();
	}
	
	void DoAddLuck() {
		if (PointsAvailable > 0) GameManager.Instance.HeroClass.CharacterLuck += 1;
		RemovePoints();
		GuiStats.Instance.Refresh();
	}
	
	void DoAddSpeed() {
		if (PointsAvailable > 0) GameManager.Instance.HeroClass.CharacterSpeed += 1;
		RemovePoints();
		GuiStats.Instance.Refresh();
	}
	
	void DoAddStrength() {
		if (PointsAvailable > 0) GameManager.Instance.HeroClass.CharacterStrength += 1;
		RemovePoints();
		GuiStats.Instance.Refresh();
	}
	
	void DoAddWeapon() {
		if (PointsAvailable > 0) GameManager.Instance.HeroClass.CharacterWeapon += 1;
		RemovePoints();
		GuiStats.Instance.Refresh();
	}
	
	void DoReady() {
		Hide();
		GameManager.Instance.CreateMap();
		GameManager.Instance.ActivateRoom(-1);
	}
	
	void RemovePoints() {
		PointsAvailable -= 1;
		TextPoints.text = string.Format("Points Available: {0}", PointsAvailable);
		if (PointsAvailable <= 0) {
			ButtonContainer.transform.localPosition = new Vector3(1000,1000,1000);
		}
	}
	
}
