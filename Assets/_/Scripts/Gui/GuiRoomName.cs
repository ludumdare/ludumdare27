﻿using UnityEngine;
using System.Collections;

public class GuiRoomName : MonoSingleton<GuiRoomName> {

	public void SetName(string name) {
		GetComponentInChildren<tk2dTextMesh>().text = name;
	}
}
