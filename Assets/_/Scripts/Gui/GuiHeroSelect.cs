﻿using UnityEngine;
using System.Collections;

public class GuiHeroSelect : MonoBehaviour {

	public tk2dUIItem	SelectorButton;
	public tk2dTextMesh HeroArmour;
	public tk2dTextMesh HeroHitpoints;
	public tk2dTextMesh HeroLuck;
	public string HeroName;
	public tk2dTextMesh HeroSpeed;
	public tk2dTextMesh HeroStrength;
	public tk2dTextMesh HeroWeapon;
	
	private int m_armour;
	private int m_hp;
	private int m_luck;
	private int m_speed;
	private int m_strenght;
	private int m_weapon;
	
	private CharacterClass m_class;
	
	
	void OnEnable() {
		SelectorButton.OnHoverOver += OnHoverOver;
		SelectorButton.OnHoverOut += OnHoverOut;
		SelectorButton.OnClick += OnClick;
	}
	
	void OnDisable() {
		SelectorButton.OnHoverOver -= OnHoverOver;	
		SelectorButton.OnHoverOut -= OnHoverOut;
		SelectorButton.OnClick -= OnClick;
	}
	
	// Use this for initialization
	void Start () {
		
		m_class = new CharacterClass();
		m_class.Create();
		
		HeroSpeed.text = string.Format("Speed ({0})", m_class.CharacterSpeed);
		HeroArmour.text = string.Format("Armour ({0})", m_class.CharacterArmour);
		HeroHitpoints.text = string.Format("Hit Points ({0})", m_class.CharacterHitPoints);
		HeroLuck.text = string.Format("Luck ({0})", m_class.CharacterLuck);
		HeroStrength.text = string.Format("Strength ({0})", m_class.CharacterStrength);
		HeroWeapon.text = string.Format("Weapon ({0})", m_class.CharacterWeapon);
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnHoverOver() {
		Log.Normal("hover over");
		SelectorButton.GetComponentInChildren<tk2dSlicedSprite>().color = new Color32(255,255,255,25);
	}
	
	void OnHoverOut() {
		Log.Normal("hover out");
		SelectorButton.GetComponentInChildren<tk2dSlicedSprite>().color = new Color32(255,255,255,0);
	}
	
	void OnClick() {
		m_class.CharacterPortrait = HeroName;
		m_class.CharacterSpriteId = HeroName;
		GameManager.Instance.HeroClass = m_class;
		Application.LoadLevel("Game");
	}
}
