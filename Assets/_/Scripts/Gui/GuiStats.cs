﻿using UnityEngine;
using System.Collections;

public class GuiStats : MonoSingleton<GuiStats> {
	
	public tk2dTextMesh TextArmor;
	public tk2dTextMesh TextStrength;
	public tk2dTextMesh TextHitpoints;
	public tk2dTextMesh TextSpeed;
	public tk2dTextMesh TextWeapon;
	public tk2dTextMesh TextLuck;
	public tk2dTextMesh TextLives;
	public tk2dTextMesh TextLevel;
	public tk2dSprite SpritePortrait;
	public tk2dTextMesh TextHiscore;
	
	public void Refresh() {
	
		TextArmor.text = string.Format("Armor ({0})", GameManager.Instance.HeroClass.CharacterArmour);
		TextHitpoints.text = string.Format("Hit Points ({0})", GameManager.Instance.Hitpoints);
		TextStrength.text = string.Format("Strength ({0})", GameManager.Instance.HeroClass.CharacterStrength);
		TextSpeed.text = string.Format("Speed ({0})", GameManager.Instance.HeroClass.CharacterSpeed);
		TextLuck.text = string.Format("Luck ({0})", GameManager.Instance.HeroClass.CharacterLuck);
		TextWeapon.text = string.Format("Weapon ({0})", GameManager.Instance.HeroClass.CharacterWeapon);
		TextLevel.text = string.Format("Level {0}", GameManager.Instance.GetLevel());
		TextLives.text = string.Format("x {0}", GameManager.Instance.Lives);
		SpritePortrait.SetSprite(GameManager.Instance.HeroClass.CharacterPortrait);
		TextHiscore.text = string.Format("{0}", GameManager.Instance.HiScore);
	}
	
	
}
