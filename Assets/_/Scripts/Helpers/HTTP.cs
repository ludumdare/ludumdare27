﻿using UnityEngine;
using System.Collections;

public class HTTP : MonoSingleton<HTTP> {
	
	public delegate void HTTPRequestDelegate(WWW www);
	
	public void Request(WWW www, HTTPRequestDelegate del) {
		Log.Trivial(Log.Category.Network, string.Format("http request to {0}", www.url));
		StartCoroutine(RequestWait(www, del));
	}
	
	IEnumerator RequestWait(WWW www, HTTPRequestDelegate del) {
		yield return www;
		Log.Trivial(Log.Category.Network, string.Format("http reponse error {0}", www.error));
		del(www);
	}
	
}
