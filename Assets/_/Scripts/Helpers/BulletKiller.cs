﻿using UnityEngine;
using System.Collections;

public class BulletKiller : MonoBehaviour {

	void OnCollisionEnter(Collision collision) {
	
	
		//Log.Normal(collision.gameObject.name);
		if (collision.gameObject.tag == "Projectile") {
			Destroy(collision.gameObject);
		}
	}
}
