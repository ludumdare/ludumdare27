﻿using UnityEngine;
using System.Collections;

public class DevCreateRoom : MonoBehaviour {
	
	public tk2dSpriteCollectionData RoomSprites;
	public tk2dSpriteCollectionData EnemySprites;
	
	public int RoomWidth;
	public int RoomDepth;
	public int RoomEnemyCount;
	public int RoomEnemyTypes;
	public int RoomTile;
	
	void OnEnable() {
		if (GameManager.Instance.HeroClass == null) {
			GameManager.Instance.HeroClass = new CharacterClass();
			GameManager.Instance.HeroClass.Create();
		}
		GameObject go = (GameObject) Instantiate(GamePrefab.Instance.PrefabPlayer);
		go.name = "Player";
		GameManager.Instance.Player = go;
		GameCameraController.Instance.Target = go;
		GameObject go1 = (GameObject) Instantiate(GamePrefab.Instance.PrefabPlayerController);
		go1.name = "GamePlayerController";
		GamePlayerController.Instance.Player = go;
		GameManager.Instance.CameraController = GameObject.Find("GameCamera").GetComponentInChildren<GameCameraController>();
		GameManager.Instance.RoomBorder = GameObject.Find("RoomBorder");
		GameManager.Instance.Reset();
		GameManager.Instance.CreateMap();
		GameManager.Instance.SetCurrentRoom(GameManager.Instance.GetMap().MapRooms[0]);
		GameManager.Instance.ActivateRoom(-1);
	}
	
}
