﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	
	public float MoveDelay;
		
	private GameSprite m_sprite;
	private bool m_delay = false;
	
	// Use this for initialization
	void Start () {
		m_sprite = gameObject.GetComponentInChildren<GameSprite>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (m_delay == true) return;
		//m_sprite.MoveBy(Random.Range(1,5));		
		m_sprite.MoveToRandom();
		StartCoroutine(Delay ());	
		m_delay = true;
	}
	
	public IEnumerator Delay() {
		yield return new WaitForSeconds(MoveDelay);
		m_delay = false;
	}
	
}
