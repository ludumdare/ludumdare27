﻿using UnityEngine;
using System.Collections;

public class EnemySeeker : MonoBehaviour {
	
	public float MoveDelay;

	private GameSprite m_sprite;
	private GameObject m_player;
	private bool m_delay = false;

	// Use this for initialization
	void Start () {
		m_sprite = gameObject.GetComponentInChildren<GameSprite>();
		m_player = GameObject.Find("Player");
	}
	
	public IEnumerator Delay() {
		yield return new WaitForSeconds(MoveDelay);
		m_delay = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (m_delay == true) return;
		//Log.Trivial("Move");
		m_sprite.MoveTo(m_player.transform.position);
		StartCoroutine(Delay ());	
		m_delay = true;
	}
	void OnCollisionEnter(Collision collision) {
	
		//Log.Normal("Hit");
		if (collision.gameObject.tag == "Projectile" ) {
			Destroy(collision.gameObject);
			GameManager.Instance.KillMonster(gameObject);
		}
	}
}
