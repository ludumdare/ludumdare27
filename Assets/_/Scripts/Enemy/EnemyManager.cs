﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour {
	
	public int HitPoints;
	public bool IsBoss;
	
	void OnCollisionEnter(Collision collision) {
	
		//Log.Normal("Hit");
		if (collision.gameObject.tag == "Projectile" ) {
			Destroy(collision.gameObject);
			// calc damage
			int d = 1;
			HitPoints -= d;
			if (HitPoints <= 0) {
				Destroy(gameObject);
				if (! IsBoss) GameManager.Instance.KillMonster(gameObject);
				if (IsBoss) GameManager.Instance.LevelCompleted();
			}
		}
	}
}
