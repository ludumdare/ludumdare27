﻿using UnityEngine;
using System.Collections;

public class CharacterClass {

	public int		CharacterWeapon		{ get; set; }
	public int		CharacterHitPoints	{ get; set; }
	public int		CharacterStamina	{ get; set; }
	public int		CharacterLuck		{ get; set; }
	public int		CharacterStrength	{ get; set; }
	public int		CharacterArmour		{ get; set; }
	public int		CharacterCoins		{ get; set; }
	public string	CharacterSpriteId	{ get; set; }
	public string	CharacterPortrait	{ get; set; }
	public int 		CharacterSpeed		{ get; set; }
	
	
	public void Create() {
		CharacterWeapon = Random.Range(10,25);
		CharacterHitPoints = Random.Range(15,35);
		CharacterLuck = Random.Range(5,25);
		CharacterStrength = Random.Range(10,25);
		CharacterArmour = Random.Range(2,10);
		CharacterSpeed = Random.Range(5,15);
	}
}
