﻿using UnityEngine;
using System.Collections;

public class GameCoin : MonoBehaviour {
	
	public float	KillTime = 2;
	public int		Coins;
	
	// Use this for initialization
	void Start () {
		if (Coins > 5) GetComponentInChildren<tk2dSprite>().SetSprite("coins");
		StartCoroutine(Kill());
	}
	
	IEnumerator Kill() {
		yield return new WaitForSeconds(KillTime);
		Destroy(gameObject);
	}
}
