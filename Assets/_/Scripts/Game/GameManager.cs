﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameManager : MonoSingleton<GameManager> {

	public tk2dSpriteCollectionData RoomSprites;
	public tk2dSpriteCollectionData EnemySprites;
	public GameCameraController CameraController;
	public GameObject Player;
	//public GameObject Enemy;
	//public GameObject EnemySeeker;
	public GameObject RoomBorder;
	public GameObject BossRoomBorder;
	public int EnemiesLeft;
	public int Score;
	public int Coins;
	public int Lives;
	public int HiScore;
	public CharacterClass HeroClass { get; set; }
	public int Hitpoints;
	public string LastSubmitName;
	
	private Room m_currentRoom;
	private Map m_map;
	private int m_level = 0;
	private bool m_haskey;
	private bool m_keydropped;
	
	void Start() {
	}
	
	public void Reset() {
		Score = 0;
		Lives = 3;
		m_level = 0;
		Coins = 0;
		Hitpoints = HeroClass.CharacterHitPoints;
	}
	
	public void CreateMap() {
		GuiLevelUp.Instance.Hide();
		m_level++;
		
		m_map = new Map();
		MapLevel _maplevel = new MapLevel();
		MapBuilder _mapbuilder = new MapBuilder();
		
		_maplevel.MapRooms = GetRooms();
		_maplevel.MapBossRooms = GetBossRooms();
		_maplevel.MapMonsters = GetMonsters();
		
		m_map.Generate(_maplevel);
		
		_mapbuilder.Build(m_map);
		HasKey = false;
		m_keydropped = false;
		m_haskey = false;
		GameMusic.Instance.audio.clip = GameMusic.Instance.PrefabMusicLevel;
		GameMusic.Instance.audio.Play();
		Hitpoints = HeroClass.CharacterHitPoints;
	}
	
	public bool HasKey { get; set; }
	public int GetLevel() { return m_level; }
	
	int GetRooms() {
		
		int _return = 5;
		if (m_level > 4) _return += Random.Range(5,10); 
		if (m_level > 9) _return += Random.Range(5,10); 
		if (m_level > 19) _return += Random.Range(10,15); 
		if (m_level > 24) _return += Random.Range(20,25); 
		return _return;
	}
	
	int GetBossRooms() {
		int _return = 1;
		return _return;
	}
	
	int GetMonsters() {
		int _return = 5;
		if (m_level > 4) _return += Random.Range(10,20); 
		if (m_level > 9) _return += Random.Range(15,25); 
		if (m_level > 19) _return += Random.Range(25,35); 
		if (m_level > 24) _return += Random.Range(35,45); 
		return _return;
	}
	
	public void SetCurrentRoom(Room room) {
		m_currentRoom = room;
	}
	
	public Room GetCurrentRoom() { return m_currentRoom; }
	
	public Map GetMap() { return m_map; }
	
	public void ActivateRoom(int door) {
		
		if (Lives <= 0) EndGame();
		
		Player.GetComponentInChildren<tk2dSprite>().SetSprite(HeroClass.CharacterSpriteId);
		
		Log.Normal("Activate");
		Log.Normal(string.Format("room number {0}", m_currentRoom.RoomNumber));

		// focus camera on center of room
		float _marx = ((m_currentRoom.RoomPosition.x) * 5) * 20;
		float _mary = ((m_currentRoom.RoomPosition.y) * 5) * 20;
		float _offx = (((m_currentRoom.RoomPosition.x) * m_currentRoom.RoomWidth) * 20) + _marx;
		float _offy = (((m_currentRoom.RoomPosition.y) * m_currentRoom.RoomDepth) * 20) + _mary;
		
		//float _cx = ( (m_currentRoom.RoomPosition.x * 20) + ((m_currentRoom.RoomWidth/2) * 20) ) + _marx;
		//float _cy = ( (m_currentRoom.RoomPosition.y * 20) + ((m_currentRoom.RoomDepth/2) * 20) ) + _mary;
		
		float _cx = ( _offx + ((m_currentRoom.RoomWidth/2) * 20) );
		float _cy = ( _offy + ((m_currentRoom.RoomDepth/2) * 20) );

		Vector3 _pos = new Vector3(_cx, _cy, -5);
		Player.transform.position = _pos;
		//GameObject Player = (GameObject) Instantiate(GamePrefab.Instance.PrefabPlayer);
		//Player.name= "Player";
		
		SetPlayerPosition(door);
		CameraController.MinX = _cx + 130;
		CameraController.MaxX = _cx + 240;
		CameraController.MinY = _cy - 40;
		CameraController.MaxY = _cy + 100;
		RoomBorder.transform.position = new Vector3(_pos.x+40, _pos.y+40,-5);
		m_currentRoom.RoomTopPos = new Vector2(_offx+40, _offy+40);
		m_currentRoom.RoomBotPos = new Vector2(_offx + (m_currentRoom.RoomWidth * 20)+20, _offy + (m_currentRoom.RoomDepth * 20)+20);
		Player.gameObject.GetComponentInChildren<GameSprite>().SetRoom(m_currentRoom);
		GuiRoomName.Instance.SetName(m_currentRoom.RoomName);
	
		// add doors
		
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("Door")) {
			Destroy(obj);
		}
		
		if ( m_currentRoom.RoomDoors[0] ) {
			GameObject go = (GameObject) Instantiate(GamePrefab.Instance.PrefabDoor);
			go.GetComponentInChildren<GameDoor>().NextRoom = m_currentRoom.RoomAttached[0];
			go.GetComponentInChildren<GameDoor>().DoorNumber = 0;
			if (m_currentRoom.RoomAttached[0].RoomBoss) {
				go.GetComponentInChildren<tk2dSprite>().SetSprite("door_large");
				if (m_currentRoom.RoomComplete) go.GetComponentInChildren<GameDoor>().UnlockDoor();
			} else {
				if (m_currentRoom.RoomNumber == 0) go.GetComponentInChildren<GameDoor>().UnlockDoor();
				if (m_currentRoom.RoomComplete) go.GetComponentInChildren<GameDoor>().UnlockDoor();
			}
			go.transform.position = new Vector3(_offx + ((m_currentRoom.RoomWidth/2) * 20)+20, _offy + (m_currentRoom.RoomDepth * 20)+20, -1);
		}
		
		if ( m_currentRoom.RoomDoors[3] ) {
			GameObject go = (GameObject) Instantiate(GamePrefab.Instance.PrefabDoor);
			go.GetComponentInChildren<GameDoor>().NextRoom = m_currentRoom.RoomAttached[3];
			go.GetComponentInChildren<GameDoor>().DoorNumber = 3;
			if (m_currentRoom.RoomAttached[3].RoomBoss) {
				go.GetComponentInChildren<tk2dSprite>().SetSprite("door_large");
				if (m_currentRoom.RoomComplete) go.GetComponentInChildren<GameDoor>().UnlockDoor();
			} else {
				if (m_currentRoom.RoomComplete) go.GetComponentInChildren<GameDoor>().UnlockDoor();
				if (m_currentRoom.RoomNumber == 0) go.GetComponentInChildren<GameDoor>().UnlockDoor();
			}
			go.transform.position = new Vector3(_offx + 20, _offy + ((m_currentRoom.RoomDepth/2) * 20)+20, -1);
				
		}
		
		if ( m_currentRoom.RoomDoors[2] ) {
			GameObject go = (GameObject) Instantiate(GamePrefab.Instance.PrefabDoor);
			go.GetComponentInChildren<GameDoor>().NextRoom = m_currentRoom.RoomAttached[2];
			go.GetComponentInChildren<GameDoor>().DoorNumber = 2;
			if (m_currentRoom.RoomAttached[2].RoomBoss) {
				go.GetComponentInChildren<tk2dSprite>().SetSprite("door_large");
				if (m_currentRoom.RoomComplete) go.GetComponentInChildren<GameDoor>().UnlockDoor();
			} else {
				if (m_currentRoom.RoomNumber == 0) go.GetComponentInChildren<GameDoor>().UnlockDoor();
				if (m_currentRoom.RoomComplete) go.GetComponentInChildren<GameDoor>().UnlockDoor();
			}
			go.transform.position = new Vector3(_offx + ((m_currentRoom.RoomWidth/2) * 20)+20, _offy + 20, -1);
		}
		
		if ( m_currentRoom.RoomDoors[1] ) {
			GameObject go = (GameObject) Instantiate(GamePrefab.Instance.PrefabDoor);
			go.GetComponentInChildren<GameDoor>().DoorNumber = 1;
			go.GetComponentInChildren<GameDoor>().NextRoom = m_currentRoom.RoomAttached[1];
			if (m_currentRoom.RoomAttached[1].RoomBoss) {
				go.GetComponentInChildren<tk2dSprite>().SetSprite("door_large");
				if (m_currentRoom.RoomComplete) go.GetComponentInChildren<GameDoor>().UnlockDoor();
			} else {
				if (m_currentRoom.RoomNumber == 0) go.GetComponentInChildren<GameDoor>().UnlockDoor();
				if (m_currentRoom.RoomComplete) go.GetComponentInChildren<GameDoor>().UnlockDoor();
			}
			go.transform.position = new Vector3(_offx + (m_currentRoom.RoomWidth * 20)+20, _offy + ((m_currentRoom.RoomDepth/2) * 20)+20, -1);
		}
		
		
		// add monsters
		
		GameObject _e = GameObject.Find("Enemies");
		if (_e != null) Destroy(_e);
		GameObject _c = new GameObject("Enemies");
		
		int spriteid = Random.Range(0,18);
		
		if (!m_currentRoom.RoomComplete && !m_currentRoom.RoomBoss) {
			for (int i = 0; i < m_currentRoom.RoomMonsters; i++) {
				float _x = Random.Range(m_currentRoom.RoomTopPos.x+400, m_currentRoom.RoomBotPos.x-400);
				float _y = Random.Range(m_currentRoom.RoomTopPos.y+400, m_currentRoom.RoomBotPos.y-400);
				int _type = Random.Range(0,2);
				//Log.Normal(_type.ToString());
				GameObject _go = null;
				if (_type == 0) _go = (GameObject) Instantiate(GamePrefab.Instance.PrefabEnemy);
				if (_type == 1) _go = (GameObject) Instantiate(GamePrefab.Instance.PrefabEnemySeeker);
				_go.GetComponentInChildren<EnemyManager>().HitPoints = 1 + (m_level/2);
				_go.GetComponentInChildren<tk2dSprite>().spriteId = spriteid;
				_go.transform.position = new Vector3(_x, _y, -5);
				_go.GetComponentInChildren<GameSprite>().SetRoom(m_currentRoom);
				_go.transform.parent = _c.transform;
			}
			
			
		// add treasure
		
		}
		
		if (m_currentRoom.RoomBoss) {
			float _x = Random.Range(m_currentRoom.RoomTopPos.x+400, m_currentRoom.RoomBotPos.x-400);
			float _y = Random.Range(m_currentRoom.RoomTopPos.y+400, m_currentRoom.RoomBotPos.y-400);
			GameObject _go = (GameObject) Instantiate(GamePrefab.Instance.PrefabEnemyGiantBoss);
			_go.GetComponentInChildren<tk2dSprite>().spriteId = Random.Range(0,17);
			_go.transform.position = new Vector3(_x, _y, -5);
			_go.transform.parent = _c.transform;
			_go.GetComponentInChildren<EnemyManager>().HitPoints = 10 + ((m_level/2) * 10);
			_go.GetComponentInChildren<EnemyManager>().IsBoss = true;
		}
		
		
		EnemiesLeft = m_currentRoom.RoomMonsters;
		
		// set player stats and start timer
		GuiStats.Instance.Refresh();
		Player.GetComponentInChildren<GameSprite>().Speed = 2 + ( HeroClass.CharacterSpeed / 10 );
		GamePlayerController.Instance.ShootDelay = .5f - (HeroClass.CharacterWeapon * .01f);
		if (GamePlayerController.Instance.ShootDelay < .05f) GamePlayerController.Instance.ShootDelay = .05f;
		GameTimer.Instance.TimerStart();
	}
	
	// focus camera on the active room
	public void FocusCamera(Vector3 pos) {
		HOTween.To(Camera.main.transform, 1, "position", pos);
	}
	
	public void SetPlayerPosition(int door) {
		
		Log.Normal("last door was " + door);
		
		// focus camera on center of room
		float _marx = ((m_currentRoom.RoomPosition.x) * 5) * 20;
		float _mary = ((m_currentRoom.RoomPosition.y) * 5) * 20;
		float _offx = (((m_currentRoom.RoomPosition.x) * m_currentRoom.RoomWidth) * 20) + _marx;
		float _offy = (((m_currentRoom.RoomPosition.y) * m_currentRoom.RoomDepth) * 20) + _mary;
		
		// 
		
		switch (door) 
		{
		case 0:
			Player.transform.position = new Vector3(_offx + ((m_currentRoom.RoomWidth/2) * 20) + 20, _offy + 20 + 100, 0);
			break;
		case 1:
			Player.transform.position = new Vector3(_offx + 20 + 60, _offy + ((m_currentRoom.RoomDepth/2) * 20)+20, -1);
			break;
		case 2:
			Player.transform.position = new Vector3(_offx + ((m_currentRoom.RoomWidth/2) * 20) + 20, _offy + (m_currentRoom.RoomDepth * 20) + 20 - 100, 0);
			break;
		case 3:
			Player.transform.position = new Vector3(_offx + (m_currentRoom.RoomWidth * 20) + 20 - 60, _offy + ((m_currentRoom.RoomDepth/2) * 20)+20, -1);
			break;
		}
	}
	
	public void KillMonster(GameObject go) {
		EnemiesLeft -= 1;
		Log.Watch("enemies", EnemiesLeft);
		Score += 10;
		GameScore.Instance.SetScore(Score);
		GameObject coin = (GameObject) Instantiate(GamePrefab.Instance.PrefabCoin, go.transform.position, Quaternion.identity);
		coin.GetComponentInChildren<GameCoin>().Coins = Random.Range(1, (m_level/2) + HeroClass.CharacterLuck);
		Destroy(go,0);
		if ( EnemiesLeft <= 0 ) {
			GameManager.Instance.GetCurrentRoom().RoomComplete = true;
			foreach(GameObject obj in GameObject.FindGameObjectsWithTag("Door")) {
				obj.GetComponentInChildren<GameDoor>().UnlockDoor();
			}
			bool _allcompleted = true;
			foreach(Room room in m_map.MapRooms) {
				if (!room.RoomBoss) {
					Log.Normal(string.Format("room {0} is completed {1}", room.RoomName, room.RoomComplete));
					if (!room.RoomComplete) _allcompleted = false;	
				}
			}
			if (_allcompleted & !m_keydropped) {
				GameObject _go = (GameObject) Instantiate(GamePrefab.Instance.PrefabTreasureKey, go.transform.position, Quaternion.identity);
				_go.name = "TreasureKey";
				m_keydropped = true;
			}
		}
	}
	
	public void TimerComplete() {
		KillPlayer();
	}
	
	private void KillPlayer() {
		foreach(GameObject obj in GameObject.FindGameObjectsWithTag("Door")) {
			Destroy(obj);
		}
		StartCoroutine(KillPlayerSeq());		
	}

	IEnumerator KillPlayerSeq() {
		Destroy(GameObject.Find("Enemies"));
		GameObject go = (GameObject) Instantiate(GamePrefab.Instance.PrefabFakeBoss);
		go.transform.position = Player.transform.position;
		audio.clip = GameSound.Instance.BossRoar;
		audio.Play();
		yield return new WaitForSeconds(5);
		Lives -= 1;
		Destroy(go);
		m_currentRoom = m_map.MapRooms[0];
		ActivateRoom(-1);
	}
	
	public void LevelCompleted() {
		Log.Normal("level completed");
		audio.clip = GameSound.Instance.PlayerWin;
		audio.Play();
		GameTimer.Instance.TimerStop();
		if (m_level >= 29) {
			Application.LoadLevel("Win");	
		}
		GuiLevelUp.Instance.PointsAvailable = Mathf.CeilToInt(m_level*.35f) + 5;
		GuiLevelUp.Instance.Show();
	}
	
	public void AddScore(int points) {
		Score += points;
		GameScore.Instance.SetScore(Score);
	}
	
	public void AddCoins(int coins) {
		Coins += coins;
		Score += coins * 5;
		GameScore.Instance.SetScore(Score);
	}
	
	public void PlayerDamage(int damage) {
		Hitpoints -= damage;
		GuiStats.Instance.Refresh();		
		if (Hitpoints <=0) {
			Hitpoints = HeroClass.CharacterHitPoints;
			GameTimer.Instance.TimerStop();
			StartCoroutine(KillPlayerSeq());	
			
		}
	}
	public void EndGame() {
		Application.LoadLevel("End");
	}
}
