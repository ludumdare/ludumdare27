// Copyright 2012, SpockerDotNet LLC

using UnityEngine;
using System.Collections;

/**
 * Controls Game Timer
 * 
 * This class controls the management of the Game Timer for a level.
 **/

/// <summary>
/// Controls the Game Timer
/// 
/// <para></para>
/// 
/// </summary>
/// 
public class GameTimer : MonoSingleton<GameTimer> {
	
	public string				Current;
	public int					Duration = 30;
	
	private int					m_duration;
	private bool				m_started;
	
	private float				m_startTime;
	
	private tk2dTextMesh		m_text;
	
	void Start () {
		
		m_duration = Duration;
		
		// get MaxGameTimerText object
		
		m_text = GetComponentInChildren<tk2dTextMesh>();
		
		if (m_text == null) {
			Log.Critical(string.Format("GameTimer is missing TextMesh Component"));
			return;
		}
		
	}
	
	
	// Update is called once per frame
	void Update () {
		if (m_started) {
			DoTimer();
		}
	}
	
	void DoTimer() {
		
		float _timeLeft = Time.time - m_startTime;
		float _rsec = m_duration - _timeLeft;
		int _rseconds = Mathf.CeilToInt(_rsec);
		
		if (_rseconds < 0 ) {
			_rseconds = 0;
			m_started = false;
			GameManager.Instance.TimerComplete();
		}
		
		int _sec = _rseconds % 60;
		int _min = _rseconds / 60;
		
		string _formattedTime = System.String.Format("{0:0}:{1:00}", _min, _sec);
		Current = _formattedTime;
		m_text.text = _formattedTime;
		
	}

	public void TimerStart() {
		m_duration = Duration;
		m_started = true;
		m_startTime = Time.time;
	}	
	
	public void TimerStop() {
		m_started = false;
	}
	
	public void TimerSet(int seconds) {
		m_started = false;
		m_duration = seconds;
		Duration = m_duration;
		FormatTime(seconds);
	}
	
	void FormatTime(int seconds) {
		
		int _sec = seconds % 60;
		int _min = seconds / 60;
		
		string _formattedTime = System.String.Format("{0:0}:{1:00}", _min, _sec);
		Current = _formattedTime;
		m_text.text = _formattedTime;
		
	}
	
}
