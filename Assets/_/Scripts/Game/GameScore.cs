﻿using UnityEngine;
using System.Collections;

public class GameScore : MonoSingleton<GameScore> {
	
	public void SetScore(int score) {
		GetComponentInChildren<tk2dTextMesh>().text = score.ToString();
	}
}
