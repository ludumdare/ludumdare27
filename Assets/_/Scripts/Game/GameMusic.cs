﻿using UnityEngine;
using System.Collections;

public class GameMusic : MonoSingleton<GameMusic> {

	public AudioClip	PrefabMusicTitle;
	public AudioClip	PrefabMusicLevel;
	public AudioClip	PrefabMusicBoss;
	public AudioClip	PrefabMusicClear;
	public AudioClip	PrefabMusicGameEnd;
	public AudioClip	PrefabMusicGameOver;
	public AudioClip	PrefabMusicStart;
}
