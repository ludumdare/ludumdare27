﻿using UnityEngine;
using System.Collections;

public class GameSound : MonoSingleton<GameSound> {
	
	public AudioClip	EnemyHit;
	public AudioClip	PlayerHit;
	public AudioClip	BossRoar;
	public AudioClip	BossHit;
	public AudioClip	BossDie;
	public AudioClip	PlayerKey;
	public AudioClip	PlayerDie;
	public AudioClip	PlayerWin;
	public AudioClip	PlayerTime;
	public AudioClip	PlayerCoins;
}
