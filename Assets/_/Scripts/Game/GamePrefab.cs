﻿using UnityEngine;
using System.Collections;

/**
 * 	Use this class to store all the prefabs for the game.
 * 
 *  
 * 
 **/

public class GamePrefab : MonoSingleton<GamePrefab> {
	
	public GameObject		PrefabDoor;
	public GameObject		PrefabEnemy;
	public GameObject		PrefabEnemySeeker;
	public GameObject		PrefabEnemyGiantBoss;
	public GameObject		PrefabFakeBoss;
	public GameObject		PrefabPlayer;
	public GameObject		PrefabPlayerMissle;
	public GameObject		PrefabPlayerController;
	public GameObject 		PrefabTreasureKey;
	public GameObject		PrefabCoin;
	
}
