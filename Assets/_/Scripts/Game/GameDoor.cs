﻿using UnityEngine;
using System.Collections;

public class GameDoor : MonoBehaviour {
	
	public Room NextRoom;
	public int DoorNumber;
	
	private bool m_touched = false;
	private bool m_locked = true;
	
	void OnTriggerEnter(Collider other) {
	
		GameTimer.Instance.TimerStop();
		if (m_touched) return;
		if (m_locked) return;
		//Destroy(other.gameObject);
		Log.Normal("door touched");
		Log.Normal("next room is " + NextRoom.RoomName);
		GameManager.Instance.SetCurrentRoom(NextRoom);
		GameManager.Instance.ActivateRoom(DoorNumber);
		m_touched = true;
		
	}
	
	public void UnlockDoor() {
		if (!NextRoom.RoomBoss) {
			GetComponentInChildren<tk2dSprite>().SetSprite("door_small_open");
			m_locked = false;
		}
		if (NextRoom.RoomBoss & GameManager.Instance.HasKey) {
			GetComponentInChildren<tk2dSprite>().SetSprite("door_large_open");
			m_locked = false;
		}
	}
}
