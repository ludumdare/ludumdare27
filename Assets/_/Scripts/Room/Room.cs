﻿using UnityEngine;
using System.Collections;

public class Room {

	public string 		RoomName 		{ set; get; }	// random name of the room
	public int	 		RoomWidth 		{ set; get; }	// width
	public int			RoomDepth		{ set; get; }	// depth
	public RoomStyle 	RoomTheme 		{ get; set; }	// theme/style to use (sets the tileset)
	public Vector2		RoomPosition	{ get; set; }	// position on the map
	public Vector2		RoomTopPos		{ get; set; }   // top position
	public Vector2		RoomBotPos		{ get; set; }   // top position
	public bool[]		RoomDoors		{ get; set; }	// door available -> starting from 1 going clockwise
	public Room[]		RoomAttached	{ get; set; }	// rooms attached by doors
	public int			RoomMonsters	{ get; set; }	// max monsters
	public bool			RoomComplete	{ get; set; }	// is the room completed?
	public int	 		RoomNumber		{ get; set; }
	public bool			RoomBoss		{ get; set; }
	
	public Room() {
		RoomDoors = new bool[4];	
		RoomAttached = new Room[4];
	}
	
}
