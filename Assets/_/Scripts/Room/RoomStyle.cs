﻿using UnityEngine;
using System.Collections;

public class RoomStyle {

	public string 					StyleName 				{ get; set; }
	public tk2dSpriteCollectionData StyleSpriteCollection 	{ get; set; }
	public tk2dSpriteCollectionData	StyleWalls				{ get; set; }
	public int						StyleStartingId			{ get; set; }
}
